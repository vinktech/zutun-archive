import enum
from datetime import datetime
from typing import Any, Dict, Optional, Set

from dateutil.tz import tz
from tasklib import Task

from vinktech.abstracts import VJson
from zutun.dates import Dates
from zutun.level import LEVELS, Level


class EtwasState(enum.Enum):
    Due = "due"
    Completed = "completed"
    Deleted = "deleted"


def age_str(age: int) -> str:
    if -1 > age:
        return f"in {-1 * age} days"
    if -1 == age:
        return "in 1 day"
    if 0 == age:
        return "today"
    if 1 == age:
        return "1 day ago"
    return f"{age} days ago"


class Etwas(VJson):
    def __init__(self, task: Task):
        self.task: Task = task
        self.state: EtwasState = EtwasState.Due
        if self.task.completed:
            self.state = EtwasState.Completed
        elif self.task.deleted:
            self.state = EtwasState.Deleted

        if EtwasState.Due == self.state:
            self.age: int = Dates.days_until(self.due)
        else:
            self.age: int = Dates.days_since(self.done)

        self.level: Level = LEVELS.determine(
            self.age if EtwasState.Due == self.state else LEVELS["new"].max_age_days - 1
        )

    @property
    def text(self) -> str:
        return self.task["description"]

    @property
    def tags(self) -> Set[str]:
        return set(self.task["tags"])

    @property
    def created(self) -> datetime:
        return self.task["entry"]

    @property
    def due(self) -> datetime:
        return self.task["due"] if self.task["due"] is not None else datetime.now(tz.tzlocal())

    @property
    def done(self) -> Optional[datetime]:
        if EtwasState.Due != self.state:
            return self.task["modified"]
        return None

    @property
    def comment(self) -> Optional[str]:
        if 0 < len(self.task["annotations"]):
            return self.task["annotations"][0]
        return None

    def filter(self, level: Level, age: Optional[int]) -> bool:
        if self.level < level:
            return False
        if age is not None and self.age < age:
            return False
        return True

    def save(self):
        self.task.save()

    def complete(self):
        self.task.done()
        self.state = EtwasState.Completed
        self.save()

    def delete(self):
        self.task.delete()
        self.state = EtwasState.Deleted
        self.save()

    def dict(self) -> Dict[str, Any]:
        return {
            "id": self.task["id"],
            "task": self.text,
            "state": self.state.value,
            "tags": list(self.tags),
            "due": Dates.strf(self.due),
            "created": Dates.strf(self.created),
            "done": Dates.strf(self.done),
            "comment": self.comment,
        }

    def cli(self, extras: Dict[str, Any], max_task_length: int) -> str:
        spacer: str = " "
        id_: str = self.task["uuid"] if extras.get("uuid", False) else self.task["id"]
        len_id: int = len(self.task["uuid"]) + 5 if extras.get("uuid", False) else 5
        if EtwasState.Completed == self.state:
            id_ = "C"
        if EtwasState.Deleted == self.state:
            id_ = "D"
        level: str = self.level.name
        task: str = self.text
        # noinspection PyTypeChecker
        state: str = self.state.value
        age: str = age_str(self.age)
        due: str = Dates.strf(self.due, print_=True)
        created: str = Dates.strf(self.created, print_=True)
        done: Optional[str] = None if self.done is None else Dates.strf(self.done, print_=True)
        tags: Optional[str] = None if not self.tags else ", ".join(self.tags)
        comment: Optional[str] = self.comment

        fmt: str = "{id_:<" + str(len_id) + "}"
        if extras.get("level", False):
            fmt += "{level:<13}"
        fmt += "{task:<" + str(max_task_length) + "} {state:>11} "
        if extras.get("due", False):
            fmt += "{age:<15}(on {due}){spacer:<5}"
        else:
            fmt += "{age:<15}"

        if extras.get("created", False):
            fmt += "created: {created}{spacer:<4}"

        if extras.get("done", False):
            if done is not None:
                fmt += "done: {done}{spacer:<4}"
            else:
                fmt += "{spacer:<26}"

        if extras.get("tags", False):
            fmt += "tags: {tags}"

        if extras.get("comment", False) and comment is not None:
            fmt += "{spacer:>4}comment: {comment}"

        text: str = eval(f'f"{fmt}"')
        return self.level.colour.wrap_cli(text)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, type(self)):
            return (
                self.task == other.task
                and self.tags == other.tags
                and self.state == other.state
                and self.due == other.due
            )
        return False

    def __ge__(self, other: object) -> bool:
        if isinstance(other, type(self)):
            if self.state != other.state:
                return EtwasState.Due != self.state
            if self.age != other.age:
                return self.age < other.age
            return self.created >= other.created

        raise ValueError(f"{other} is not an Etwas instance")

    def __gt__(self, other: object) -> bool:
        if isinstance(other, type(self)):
            if self.state != other.state:
                return EtwasState.Due != self.state
            if self.age != other.age:
                return self.age < other.age
            return self.created > other.created
        raise ValueError(f"{other} is not an Etwas instance")

    def __le__(self, other: object) -> bool:
        if isinstance(other, type(self)):
            if self.state != other.state:
                return EtwasState.Due == self.state
            if self.age != other.age:
                return self.age > other.age
            return self.created <= other.created
        raise ValueError(f"{other} is not an Etwas instance")

    def __lt__(self, other: object) -> bool:
        if isinstance(other, type(self)):
            if self.state != other.state:
                return EtwasState.Due == self.state
            if self.age != other.age:
                return self.age > other.age
            return self.created < other.created
        raise ValueError(f"{other} is not an Etwas instance")
