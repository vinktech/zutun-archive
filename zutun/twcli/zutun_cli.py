from typing import Any, Dict, List, Optional

from tasklib import Task, TaskWarrior

from zutun.etwas import Etwas
from zutun.level import LEVELS, Level
from zutun.twcli.parser_cli import ZuTunCliParser


class ZuTunCli:
    def __init__(self) -> None:
        self.tw: TaskWarrior = TaskWarrior(create=True)
        self.parser: ZuTunCliParser = ZuTunCliParser(self.tw)

    def add(self, args, already_complete):
        task: Task = self.parser.parse(args)
        task.save()
        if already_complete:
            task.done()
            task.save()

    def complete(self, kwargs: Dict[str, Any], comment: str):
        task: Task = self.tw.tasks.get(**kwargs)
        if comment is not None:
            task.add_annotation(comment)
        task.done()
        task.save()

    def delete(self, kwargs: Dict[str, Any], comment: str):
        task: Task = self.tw.tasks.get(**kwargs)
        if comment is not None:
            task.add_annotation(comment)
        task.delete()
        task.save()

    def list(self, args: Dict[str, Any]):
        etwasse: List[Etwas] = []
        tags: Optional[str] = None if "tagsf" not in args else " ".join(args.pop("tagsf"))
        filter_args: Dict[str, Any] = {}
        if "completed" in args:
            filter_args["end"] = args.pop("completed")
        modifier: str = args.pop("modifier", "pending")
        if "all" != modifier:
            filter_args["status"] = modifier
        for task in self.tw.tasks.filter(tags, **filter_args):
            etwasse.append(Etwas(task))
        level: Level = LEVELS[args.pop("levelf", "future")]
        age: Optional[int] = args.pop("age", None)
        filtered: List[Etwas] = [etwas for etwas in etwasse if etwas.filter(level, age)]
        if 0 == len(filtered):
            print("No tasks")
            return
        max_task_length: int = len(max(filtered, key=lambda e: len(e.text)).text)
        filtered.sort()
        for etwas in filtered:
            print(etwas.cli(args, max_task_length))
