import os
from typing import List

from zutun.twcli.hooks.git import GitHook
from zutun.twcli.hooks.hook import Hook


class HookManager:
    def __init__(self):
        self.hooks: List[Hook] = [GitHook()]

    def run(self, hooks_dir: str):
        hooks_dir = os.path.expanduser(hooks_dir)
        os.makedirs(hooks_dir, exist_ok=True)
        for hook in self.hooks:
            hook.apply(hooks_dir)
