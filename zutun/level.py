from typing import Dict, Iterator, List

from vinktech.abstracts import VCli
from zutun.colour import Colour, Colours


class Level(VCli):
    def __init__(self, name: str, max_age_days: int, colour: Colour = None) -> None:
        self.name: str = name
        self.max_age_days: int = max_age_days
        self.colour: Colour = colour

    def accepts(self, age_days: int) -> bool:
        return age_days < self.max_age_days

    def cli(self) -> str:
        if self.colour:
            return self.colour.wrap_cli(self.__repr__())
        return self.__repr__()

    def __repr__(self) -> str:
        return f"Level({self.name}, {self.max_age_days} days)"

    def __eq__(self, o: object) -> bool:
        if isinstance(o, type(self)):
            return o.max_age_days == self.max_age_days
        return False

    def __ge__(self, o: object) -> bool:
        if isinstance(o, type(self)):
            return o.max_age_days <= self.max_age_days
        raise ValueError(f"{o} is not a Taak Level instance")

    def __gt__(self, o: object) -> bool:
        if isinstance(o, type(self)):
            return o.max_age_days < self.max_age_days
        raise ValueError(f"{o} is not a Taak Level instance")

    def __le__(self, o: object) -> bool:
        if isinstance(o, type(self)):
            return o.max_age_days >= self.max_age_days
        raise ValueError(f"{o} is not a Taak Level instance")

    def __lt__(self, o: object) -> bool:
        if isinstance(o, type(self)):
            return o.max_age_days > self.max_age_days
        raise ValueError(f"{o} is not a Taak Level instance")


class Levels(VCli):
    def __init__(self, *levels: Level):
        self.list: List[Level] = list(levels)
        self.list.sort(reverse=True)
        self.dict: Dict[str, Level] = {l.name: l for l in self.list}
        self.str: str = "\n".join([l.cli() for l in self.list])

    def cli(self) -> str:
        return self.str

    def determine(self, age: int) -> Level:
        level: Level = self["urgent"]
        for l in self:
            if not l.accepts(age):
                return level
            level = l
        return level

    def __getitem__(self, level: str) -> Level:
        return self.dict[level]

    def __iter__(self) -> Iterator[Level]:
        return self.list.__iter__()


LEVELS: Levels = Levels(
    Level("future", 0, Colours.Grey.value),
    Level("new", 1, Colours.NoColour.value),
    Level("normal", 3, Colours.Blue.value),
    Level("a-bit-tardy", 5, Colours.Green.value),
    Level("important", 7, Colours.Yellow.value),
    Level("urgent", 10, Colours.Red.value),
)

if __name__ == "__main__":
    print(LEVELS.cli())
