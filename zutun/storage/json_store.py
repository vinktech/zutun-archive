import json
import os
from typing import Any, Dict, List, Set

from zutun.dates import Dates
from zutun.etwas import Etwas, EtwasState
from zutun.storage.store import EtwasStore


class JsonStore(EtwasStore):
    def __init__(self, dir_: str):
        self.dir: str = dir_
        self.files: Dict[EtwasState, str] = {
            state: f"{self.dir}/{state.value}.yaml" for state in EtwasState
        }

    def append(self, etwas: Etwas):
        target: str = self.files[etwas.state]
        with open(target, "a") as target_file:
            target_file.write(etwas.json())
            target_file.write("\n")

    def remove(self, etwas: Etwas):
        self.save([in_file for in_file in self.load(etwas.state) if etwas.id != in_file.id])

    def save(self, etwasse: List[Etwas]):
        if 0 < len(etwasse):
            target: str = self.files[etwasse[0].state]
            with open(target, "w") as target_file:
                for etwas in etwasse:
                    target_file.write(etwas.json())
                    target_file.write("\n")

    def load(self, state: EtwasState) -> List[Etwas]:
        target: str = self.files[state]
        if os.path.isfile(target):
            with open(target, "r") as target_file:
                etwasse: List[Etwas] = []
                for line in target_file.readlines():
                    data: Dict[str, Any] = json.loads(line)
                    etwasse.append(
                        Etwas(
                            task=data["task"],
                            state=EtwasState(data["state"]),
                            tags=set(data["tags"]),
                            due=Dates.strpdt(data["due"]),
                            created=Dates.strpdt(data["created"]),
                            done=Dates.strpdt(data.get("done")),
                            id_=data["id"],
                        )
                    )
                JsonStore.ensure_safe_short_ids(etwasse)
                return etwasse
        return []

    @staticmethod
    def ensure_safe_short_ids(etwasse: List[Etwas]):
        length: int = 10
        ok: bool = False
        while not ok:
            ids: Set[str] = set()
            for etwas in etwasse:
                if etwas.id in ids:
                    length += 1
                    ok = False
                    break
                ok = True
                ids.add(etwas.id)
            if not ok:
                for etwas in etwasse:
                    etwas.update_short_id(length)
