from datetime import datetime, timedelta
from typing import List, Optional, Tuple

from dateparser import parse
from dateparser.search import search_dates
from dateutil import tz

SECONDS_IN_DAY: int = 60 * 60 * 24


class Dates:
    ISO_DATETIME_FORMAT: str = "%Y-%m-%d %H:%M:%S.%f %z"
    PRINT_ISO_DATE_FORMAT: str = "%Y-%m-%d @ %H:%M"

    @staticmethod
    def strpdt(when: str) -> Optional[datetime]:
        if not when:
            return None
        return parse(when, settings={"RETURN_AS_TIMEZONE_AWARE": True})

    @staticmethod
    def strf(when: Optional[datetime], print_: bool = False) -> Optional[str]:
        if not when:
            return None
        return when.strftime(Dates.PRINT_ISO_DATE_FORMAT if print_ else Dates.ISO_DATETIME_FORMAT)

    @staticmethod
    def days_until(due: datetime) -> int:
        due_utc: datetime = due.astimezone(tz.tzutc())
        now_utc: datetime = datetime.now(tz.tzutc())
        return int((now_utc - due_utc).total_seconds() / SECONDS_IN_DAY)

    @staticmethod
    def days_since(done: datetime) -> int:
        done_utc: datetime = done.astimezone(tz.tzutc()).replace(
            hour=0, minute=0, second=0, microsecond=0
        )
        now_utc: datetime = datetime.now(tz.tzutc()).replace(
            hour=0, minute=0, second=0, microsecond=0
        )
        return int((now_utc - done_utc).total_seconds() / SECONDS_IN_DAY)

    @staticmethod
    def localnow():
        return datetime.now(tz.tzlocal())

    @staticmethod
    def parse_dates(text: str) -> List[Tuple[str, datetime]]:
        dates: List[Tuple[str, datetime]] = search_dates(
            text, settings={"RETURN_AS_TIMEZONE_AWARE": True}
        )
        return dates

    @staticmethod
    def ensure_due_datetime(due: datetime):
        if 0 == due.hour and 0 == due.minute and 0 == due.second and 0 == due.microsecond:
            return due.replace(hour=23, minute=59, second=59)
        return due

    @staticmethod
    def next_due_datetime(created: datetime):
        due: datetime = created.replace(hour=23, minute=59, second=59)
        if 60 > (due - created).seconds:
            return due + timedelta(days=1)
        return due
