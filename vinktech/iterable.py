from copy import copy
from typing import Any, List


class VListerator:
    def __init__(self, data: List[Any]) -> None:
        self.data: List[Any] = copy(data) if data else []
        self.count: int = len(self.data)
        self.index: int = 0

    def has_next(self) -> bool:
        return self.index < self.count

    def next(self) -> Any:
        val: Any = self.data[self.index]
        self.index += 1
        return val

    def reverse(self, count: int = 1):
        self.index -= count
        if self.index < 0:
            self.index = 0

    def reset(self):
        self.index = 0

    def __getitem__(self, index: int) -> Any:
        return self.data[index]

    def __repr__(self) -> str:
        return None if self.data is None else self.data.__repr__()
