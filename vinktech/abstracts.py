import hashlib
import json
from abc import ABC, abstractmethod
from typing import Any, Dict


class V(ABC):
    def __repr__(self):
        return f"{self.__class__.__name__}{self.__str__()}"

    def __eq__(self, other: object) -> bool:
        return self.__hash__() == other.__hash__()

    def __hash__(self) -> int:
        return hash(self.__repr__())


class VHash(V, ABC):
    def hash(self) -> str:
        return hashlib.sha256(self.__repr__().encode()).hexdigest()

    def __hash__(self) -> int:
        return hash(self.hash())


class VDict(VHash, ABC):
    @abstractmethod
    def dict(self) -> Dict[str, Any]:
        pass

    def __eq__(self, other: object) -> bool:
        if isinstance(other, type(self)):
            return other.dict() == self.dict()
        return False

    def __repr__(self):
        return f"{self.__class__.__name__}{self.dict()}"


class VJson(VDict, ABC):
    def json(self, sort_keys: bool = False) -> str:
        return json.dumps(self.dict(), sort_keys=sort_keys)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, type(self)):
            return other.json() == self.json()
        return False

    def __str__(self):
        return self.json()

    def __repr__(self):
        return f"{self.__class__.__name__}{self.json()}"


class VCli:
    @abstractmethod
    def cli(self) -> str:
        pass
