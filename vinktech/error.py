from copy import copy
from typing import Any, Dict

from vinktech.abstracts import VJson


class VError(VJson, Exception):
    def __init__(self, message: str, **kwargs):
        super().__init__(message)
        self.message: str = message
        self.kwargs: Dict[str, Any] = kwargs

    def dict(self) -> Dict[str, Any]:
        d: Dict[str, Any] = copy(self.kwargs)
        d["message"] = self.message
        return d
